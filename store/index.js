
export const state = () => ({
    blog: undefined,
    blogSlug: undefined,
    mainSliderData: undefined,
    faq: undefined,
    menu: undefined,
    page: undefined,
    reviewsList: undefined,
    trustedBys: undefined,
    teamMembers: undefined,
    blogTop: undefined,
    zips: [],
    insurances: undefined,
    vehicleYear: ["2023", "2022", "2021", "2020", "2019", "2018", "2017", "2016", "2015", "2014", "2013", "2012", "2011", "2010", "2009","2008", "2007", "2006", "2005", "2004", "2003", "2002", "2001", "2000", "1999", "1998", "1997", "1996", "1995", "1994", "1993", "1992", "1991", "1990", "1989", "1988", "1987", "1986", "1985", "1984", "1983", "1982", "1981", "1980"],
    vehicleMake: [],
    vehicleModel: [],
});

export const mutations = {
    SET_BLOG(state, blog) {
        state.blog = blog;
    },
    SET_BLOGSLUG(state, blogSlug) {
        state.blogSlug = blogSlug;
    },
    SET_MAINSLIDERDATA(state, mainSliderData) {
        state.mainSliderData = mainSliderData;
    },
    SET_FAQ(state, faq) {
        state.faq = faq;
    },
    SET_MENU(state, menu) {
        state.menu = menu;
    },
    SET_PAGE(state, page) {
        state.page = page;
    },
    SET_REVIEWS(state, reviewsList) {
        state.reviewsList = reviewsList;
    },
    SET_TRUSTEDBYS(state, trustedBys) {
        state.trustedBys = trustedBys;
    },
    SET_TEAMMEMBERS(state, teamMembers) {
        state.teamMembers = teamMembers;
    },
    SET_blogTop(state, blogTop) {
        state.blogTop = blogTop;
    },
    SET_zips(state, zips) {
        state.zips = zips
    },
    SET_vehicleMake(state, vehicleMake) {
        state.vehicleMake = vehicleMake
    },
    SET_vehicleModel(state, vehicleModel) {
        state.vehicleModel = vehicleModel
    },
    SET_INSURANCES(state, insurances) {
        state.insurances = insurances;
    },
};

export const actions = {

    async fetchBlog({ commit }, currentPage) {
        return await new Promise((resolve, reject) => {
            this.$axios
                .get(`/blog/?page=${currentPage}`, )
                .then((res) => {
                    commit("SET_BLOG", res.data);
                    resolve();
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },

    async fetchBlogSlug({ commit }, slug) {
        return await new Promise((resolve, reject) => {
            this.$axios
                .get(`/blog/${slug}`, )
                .then((res) => {
                    commit("SET_BLOGSLUG", res.data);
                    resolve();
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },

    async fetchMainSlider({ commit }) {
        return await new Promise((resolve, reject) => {
            this.$axios
                .get(`/slider/`, )
                .then((res) => {
                    commit("SET_MAINSLIDERDATA", res.data);
                    resolve();
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },

    async fetchFaq({ commit }) {
        return await new Promise((resolve, reject) => {
            this.$axios
                .get(`/faq/`, )
                .then((res) => {
                    commit("SET_FAQ", res.data);
                    resolve();
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },

    async fetchMenu({ commit }) {
        return await new Promise((resolve, reject) => {
            this.$axios
                .get(`/menus/`, )
                .then((res) => {
                    commit("SET_MENU", res.data);
                    resolve();
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },

    async fetchPage({ commit }, slug) {
        return await new Promise((resolve, reject) => {
            this.$axios
                .get(`/pages/${slug}`, )
                .then((res) => {
                    commit("SET_PAGE", res.data);
                    resolve();
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },

    async fetchReviews({ commit }) {
        return await new Promise((resolve, reject) => {
            this.$axios
                .get(`/reviews/`, )
                .then((res) => {
                    commit("SET_REVIEWS", res.data);
                    resolve();
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },

    async fetchTrustedBys({ commit }) {
        return await new Promise((resolve, reject) => {
            this.$axios
                .get(`/trusted-by/`, )
                .then((res) => {
                    commit("SET_TRUSTEDBYS", res.data);
                    resolve();
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },

    async fetchTeamMembers({ commit }) {
        return await new Promise((resolve, reject) => {
            this.$axios
                .get(`/team-members/`, )
                .then((res) => {
                    commit("SET_TEAMMEMBERS", res.data);
                    resolve();
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },

    async fetchblogTop({ commit }) {
        return await new Promise((resolve, reject) => {
            this.$axios
                .get(`/blog-top/`, )
                .then((res) => {
                    commit("SET_blogTop", res.data);
                    resolve();
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },

    async fetchZips({ commit }, key) {
        return await new Promise((resolve, reject) => {
            this.$axios
                .get(`/shipping/zip-codes/?search=${key}`, )
                .then((res) => {
                    commit("SET_zips", res.data.results);
                    resolve();
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },

    async fetchVehicleMake({ commit }, payload) {
        return await new Promise((resolve, reject) => {
            this.$axios
                .get(`/shipping/vehicles/make/?year=${payload.year}&search=${payload.key}`, )
                .then((res) => {
                    commit("SET_vehicleMake", res.data.results);
                    resolve();
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },

    async fetchVehicleModel({ commit }, payload) {
        return await new Promise((resolve, reject) => {
            this.$axios
                .get(`/shipping/vehicles/models/?make=${payload.make}&year=${payload.year}&search=${payload.key}`, )
                .then((res) => {
                    commit("SET_vehicleModel", res.data.results);
                    resolve();
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },

    async fetchInsurances({ commit }, key) {
        return await new Promise((resolve, reject) => {
            this.$axios
                .get(`/documents/?search=${key}`, )
                .then((res) => {
                    commit("SET_INSURANCES", res.data);
                    resolve();
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },
};

