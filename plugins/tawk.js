import Vue from 'vue';
import TawkMessengerVue from '@tawk.to/tawk-messenger-vue-2';

export default function () {
    Vue.use(TawkMessengerVue, {
        propertyId : '63ed39784742512879138e49',
        widgetId : '1gpbagvv0'
    });
}
