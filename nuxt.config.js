export default {
  // Global pages headers: https://go.nuxtjs.dev/config-head
  ssr: true,
  loading: {
    color: 'blue',
    height: '0px'
  },
  head: {
    title: 'Top-Rated Car Shipping & Auto Transport Company | US Star Trucking  LLC',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1',
      },
      {
        name: 'yandex-verification',
        content: '57bd747ac0319ba8',
      },
      {name: 'trustpilot-one-time-domain-verification-id', content: '9dc55dc7-9f0e-4f09-8de1-d1da8f044601'}
      // { hid: 'Need Fast & Reliable Car Shipping Service', name: 'description', content: 'Need Fast & Reliable Car Shipping Service?  Get A Quote In 3 Simple Steps! Call now 865 730-49-92 or click here to learn about our trusted services.  No Hidden Fees.' },
      // { hid: 'Auto transport company', name: 'keywords', content: 'auto transport company, car shipping company, car shipping companies, companies that ship cars, auto transport companies, car transport companies, best car shipping company, best company to ship a car, car moving companies, car hauling companies, auto shipping companies, vehicle transport company, vehicle transportation company' },
    ],
    script: [
      { src: 'https://cdn.popt.in/pixel.js?id=334f6615af8dd', id:'pixel-script-poptin' },
      { src: 'https://cdn.polygraph.net/pg.js' }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['~/assets/scss/style.scss',
    '@/static/font/stylesheet.css'
  ],

  // Plugins to run before rendering pages: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/element.js' },
    { src: '~/plugins/slider.js' },
    { src: '~/plugins/vuelidate.js' },
    { src: '~/plugins/3dSlider.js', ssr: false },
    { src: '~/plugins/Lottie.js', mode: "client" },
    { src: '~/assets/js/scripts.js' },
    { src: '~/plugins/v-mask.js' },
    { src: '~/plugins/tawk.js', mode: 'client' },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  yandexMetrika: {
    id: "87899630",
    trackLinks: true,
    accurateTrackBounce: true,
    webvisor: true
  },

  robots: {
    UserAgent: '*',
    allow: '/'
  },
  facebook: {
    /* module options */
    track: 'PageView',
    pixelId: '1321465958348907',
    autoPageView: true,
    disabled: false
  },

  gtm: {
    id: 'GTM-52TG3M9T'
  },

  publicRuntimeConfig: {
    gtm: {
      id: 'GTM-52TG3M9T'
    }
  },

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/style-resources',
    'bootstrap-vue/nuxt',
    'nuxt-facebook-pixel-module',
    '@nuxtjs/axios',
    '@nuxtjs/yandex-metrika',
    '@nuxtjs/sitemap',
    '@nuxtjs/robots',
    '@nuxtjs/gtm',
  ],

  styleResources: {
    scss: ['@/assets/scss/functions.scss'],
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: 'https://back.usstartruckingllc.com/api/',
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extractCSS: true,
    cssSourceMap: false,
  },

  sitemap: {
    hostname: 'https://www.usstartruckingllc.com'
  }
}
